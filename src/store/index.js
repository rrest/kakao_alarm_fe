import Vue from 'vue';
import Vuex from 'vuex';
import assign from 'object-assign';

import { procAjaxAlarm } from '@/assets/js/feature';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    /**
     * 로그인 사용자 정보
     */
    userInfo: null,
    /**
     * 에디트 모드
     * 0: normal
     * 1: insert
     * 2: update
     * 3: delete 
     */
    editMode: 0,
    /**
     * 페이지 정보
     */
    pageInfo: {
      size: 5,
      index: 1,
      end: 1
    },
    /**
     * 알람 리스트
     */
    alarmList: []
  },
  mutations: {
    setMode(state, type) {
      const modes = ['normal', 'insert', 'update', 'delete'];

      if (typeof type === 'number' && type > -1 && type < 4) {
        state.editMode = type;
      } else if (typeof type === 'string' && ~modes.indexOf(type)) {
        state.editMode = modes.indexOf(type);
      } else if (typeof type === 'undefined') {
        state.editMode = 0;
      } else {
        throw Error('setMode Type Error');
      }
    },
    setUserInfo(state, userInfo) {
      sessionStorage.setItem('userInfo', JSON.stringify(userInfo));

      state.userInfo = userInfo;
      state.editMode = 0;
    },
    setAlarmList(state, data) {
      state.alarmList = data;
    }, 
    insertAlarm(state, data) {
      state.alarmList.push(data);
    },
    updateAlarm(state, data) {
      for (let item of state.alarmList) {
        if (item.id === data.id) {
          assign(item, data);

          break;
        }
      }
    }, 
    deleteAlarm(state, data) {
      for (let id of data) {
        for (let idx in state.alarmList) {
          if (state.alarmList[idx].id === id) {
            state.alarmList.splice(idx, 1);
          }
        }
      }
    }
  }, 
  actions: {
    getAlarmList() {
      procAjaxAlarm('select');
    }, 
    insertAlarm(state, data) {
      procAjaxAlarm('insert', data);
    },
    updateAlarm(state, data) {
      procAjaxAlarm('update', data);
    }, 
    deleteAlarm(state, data) {
      procAjaxAlarm('delete', data);
    }
  }
});