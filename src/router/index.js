import Vue from 'vue';
import VueRouter from 'vue-router';

import Alarm from '@/components/content/alarm';
import Talk from '@/components/content/talk';

Vue.use(VueRouter);

export default new VueRouter({
  mode: 'history',
  routes: [{
    path: '/',
    component: Alarm,
    children: [{
      path: '/alarm',
      component: Alarm
    }]
  }, {
    path: '/talk',
    component: Talk
  }]
});