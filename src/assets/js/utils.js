/** 
 * @returns {string} uuid
*/
export const uuid = () => {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
    var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
};

/** 
 * @param {number} size 길이
 * @returns {string} 랜덤 해시
*/
export const hash = size => {
  const getRandomString = () => { 
    return Math.random().toString(36).slice(2); 
  };

  let str = getRandomString() || '';
  
  size = size || 6;

  while (str.length < size) {
    str += getRandomString();
  }

  return str.substr(0, size);
};

/** 
 * @param {min} min 최소값
 * @param {max} max 최대값
 * @param {floor} floor 소수점
 * @returns {number} 랜덤 숫자
*/
export const getRandomNumber = (min, max, floor) => {
  let str = '';

  if (typeof min === 'undefined' || typeof max === 'undefined') {
    min = 0;
    max = 9;
  } else if (min > max) {
    var temp = min;
    min = max;
    max = temp;
  }

  if (typeof floor === 'undefined') {
    floor = 1;
  }

  for (var i = 0; i < floor; ++i) {
    str += Math.floor((Math.random() * (max - min + 1)) + min);
  }

  return +str;
};