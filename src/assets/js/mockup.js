import { uuid, hash, getRandomNumber } from '@/assets/js/utils';
import Alarm from '@/assets/js/alarm';

export const select = size => {
  return Array.apply(null, Array(size)).map(() => {
    const obj = new Alarm();
    obj.id = hash(8);
    obj.username = 'username__' + hash(8);
    obj.subject = 'subject__' + hash(5);
    obj.message = 'message__' + hash(50);
    obj.dayOfWeek = '*';

    return obj;
  });
};

export const user = size => {
  return Array.apply(null, Array(size)).map(() => {
    return {
      avatar: [
        'https://cdn.vuetifyjs.com/images/lists/1.jpg',
        'https://cdn.vuetifyjs.com/images/lists/2.jpg',
        'https://cdn.vuetifyjs.com/images/lists/3.jpg',
        'https://cdn.vuetifyjs.com/images/lists/4.jpg',
        'https://cdn.vuetifyjs.com/images/lists/5.jpg'
      ][getRandomNumber(0, 4)],
      title: 'User__' + hash(8), 
      subtitle: '<span class="text--primary">' + hash(10) + '</span> &mdash; ' + hash(10), 
      token: uuid()
    };
  });
};

export const talkHistory = size => {
  return Array.apply(null, Array(size)).map(() => {
    return {
      avatar: [
        'https://cdn.vuetifyjs.com/images/lists/1.jpg',
        'https://cdn.vuetifyjs.com/images/lists/2.jpg',
        'https://cdn.vuetifyjs.com/images/lists/3.jpg',
        'https://cdn.vuetifyjs.com/images/lists/4.jpg',
        'https://cdn.vuetifyjs.com/images/lists/5.jpg'
      ][getRandomNumber(0, 4)],
      username: 'User__' + hash(8), 
      text: hash(getRandomNumber(5, 30)) + '__' + hash(getRandomNumber(5, 30))
    };
  });
};