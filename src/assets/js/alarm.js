export default class Alarm {
  constructor() {
    /** 
     * @name subject 알람 타이틀
     * @default ''
     * @type {string} 
     **/
    this.subject = '';
    
    /** 
     * @name message 알람 메세지
     * @default ''
     * @type {string} 
     **/
    this.message = '';

    /** 
     * @name status 상태여부
     * @default 1
     * @type {number} 
     **/
    this.status = 1;
    
    /** 
     * @name id 알람 프로세스 아이디
     * @default {hash}
     * @type {string} 
     **/
    this.id = '';

    /** 
     * @name username 사용자 (명)
     * @default 'unknown_user'
     * @type {string} 
     **/
    this.username = 'unknown_user';

    /** 
     * @name year 연도
     * @default '*'
     * @type {string} 
     **/
    this.year = '*';

    /** 
     * @name month 월
     * @default '*'
     * @type {string} 
     **/
    this.month = '*';

    // /** 
    //  * @name week 주
    //  * @default ['1째주','2째주','3째주','4째주','5째주','6째주']
    //  * @type {array} 
    //  **/
    // this.week = ['1째주', '2째주', '3째주', '4째주', '5째주', '6째주'];

    /** 
     * @name dayOfMonth 일
     * @default '?'
     * @type {string} 
     **/
    this.dayOfMonth = '?';

    /** 
     * @name dayOfWeek 요일
     * @default ['일요일','월요일','화요일','수요일','목요일','금요일','토요일']
     * @type {array} 
     **/
    this.dayOfWeek = ['일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일'];

    /** 
     * @name hours 시
     * @default '*'
     * @type {string} 
     **/
    this.hours = '*';

    /** 
     * @name minutes 분
     * @default '*'
     * @type {string} 
     **/
    this.minutes = '*';
  }
}