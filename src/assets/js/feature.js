import Vue from 'vue';
import axios from 'axios';

import store from '@/store';

const config = {
  host: 'http://alimy.choibom.com:80',
  request: {
    user: {
      path: '/auth'
    },
    select: {
      path: '/alimy', 
      args: {
        size: 30,
        page: 1, 
        sort: 'id,asc'
      }
    },
    insert: {
      path: '/alimy/save',
      args: []
    }, 
    update: {
      path: '/alimy/update', 
      args: []
    },
    delete: {
      path: '/alimy/delete',
      args: []
    } 
  }
};

const getReqUrl = (type) => {
  const host = config.host;
  const path = config.request[type].path;
  const args = config.request[type].args;

  let queryString = [];

  for (let prop in args) {
    queryString.push(prop + '=' + args[prop]);
  }

  return host + path + (queryString.length ? '?' + queryString.join('&') : '');
};

export const eventBus = new Vue();

const getTokenKey = () => {
  if (sessionStorage.getItem('userInfo')) {
    const userInfo = JSON.parse(sessionStorage.getItem('userInfo'));

    return userInfo.tokenType + ' ' + userInfo.accessToken;
  } else {
    return '';
  }
};

const procAjaxCall = (url, method = 'get', data = {}, callback) => {
  axios({ 
    method, 
    url, 
    data,
    headers: { 'Authorization': getTokenKey() }
  })
    .then(response => {
      if (+response.status === 200 && response.data) {
        callback(response.data);
      }
    })
    .catch(response => {
      /* eslint-disable no-console */
      console.error(Error(response));
      /* eslint-enable no-console */
    });
};

export const procAjaxUser = (data, callback) => {
  procAjaxCall(config.host + config.request.user.path, 'post', data, resObj => {
    store.commit('setUserInfo', resObj);

    callback();
  });
};

export const procAjaxAlarm = (type, data) => {
  const userSessionInfo = JSON.parse(sessionStorage.getItem('userInfo'));

  switch (type) {
    case 'select' : 
      procAjaxCall(getReqUrl(type), 'get', null, resObj => store.commit('setAlarmList', resObj.content));

      break;
    case 'insert' :
      data.userId = userSessionInfo.userId;
      data.userName = userSessionInfo.userName;

      procAjaxCall(getReqUrl(type), 'post', data, resObj => {
        store.commit('insertAlarm', resObj);
      });

      break;
    case 'update' : 
      procAjaxCall(getReqUrl(type), 'post', data, resObj => store.commit('updateAlarm', resObj));

      break;
    case 'delete' : 
      procAjaxCall(getReqUrl(type), 'post', {'ids': data}, () => store.commit('deleteAlarm', data));

      break;
  }
};

const parseDateFormat = list => {
  const max = Math.max.apply(null, list);
  const min = Math.min.apply(null, list);

  if ((max - min) + 1 === list.length) {
    return (min + '-' + max);
  } else {
    return list.join(',');
  }
};

export const parseDayofWeek = selectList => {
  if (selectList.length < 7) {
    return parseDateFormat(selectList.map(node => {
      return ['일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일'].indexOf(node);
    }).sort((a, b) => (a - b)));
  } else {
    return '*';
  }
};